package com.gobrs.async.def;

/**
 * The interface Default config.
 *
 * @author : wh
 * @date : 2022/6/1 11:39
 * @description:
 */
public interface DefaultConfig {

    /**
     * The constant TASKNAME.
     */
    String TASKNAME = "asyncTaskName";

    /**
     * The constant THREADPOOLQUEUESIZE.
     */
    Integer THREADPOOLQUEUESIZE = 10000;

    /**
     * The constant KEEPALIVETIME.
     */
    Long KEEPALIVETIME = 30000L;

    /**
     * The constant EXECUTETIMEOUT.
     */
    Long EXECUTETIMEOUT = 10000L;

    /**
     * The constant RULE_ANY.
     */
    String RULE_ANY = "any";
    /**
     * The constant RULE_EXCLUSIVE.
     */
    String RULE_EXCLUSIVE = "exclusive";

    /**
     * The constant retryCount.
     */
    int retryCount = 1;
    /**
     * Whether to execute a subtask if it fails
     */
    boolean failSubExec = false;
    /**
     * Transaction task
     */
    boolean transaction = false;


}
